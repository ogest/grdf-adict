<?php

declare(strict_types=1);

namespace Aeneria\GrdfAdictApi\Client;

use Aeneria\GrdfAdictApi\Model\InfoTechnique;

/**
 * Implements DataConnect Customers API
 */
interface ContratClientInterface
{
    public function requestInfoTechnique(string $accessToken, string $pce): InfoTechnique;
}
