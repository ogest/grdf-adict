<?php

declare(strict_types=1);

namespace Aeneria\GrdfAdictApi\Model;

use Aeneria\GrdfAdictApi\Exception\GrdfAdictDataNotFoundException;

class MeteringData
{
    /** @var \DateTimeImmutable */
    public \DateTimeImmutable $date;
    public float $value;
    public string $rawData;

    public static function fromJson(string $jsonData): self
    {
        $meteringData = new self();
        $meteringData->rawData = $jsonData;

        $data = \json_decode($jsonData);

        if ($data->consommation) {
            $meteringData->date = \DateTimeImmutable::createFromFormat('Y-m-d', $data->consommation->journee_gaziere);
            $meteringData->value = (float) $data->consommation->energie;
        } else {
            throw new GrdfAdictDataNotFoundException($data->statut_restitution->message ?? null);
        }

        return $meteringData;
    }
}
