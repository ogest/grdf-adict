# GRDF ADICT API: a small library to use a part of GRDF ADICT API with PHP

Firstly developped to be used in [aeneria](https://aeneria.com), this small library let you use easily
[GRDFS ADICT API](https://grdf-adict.grdf.fr).

## Requirements

PHP 8.1 or higher

## Getting started

First of all, you will need a ClientID and a ClientSecret to use GRDF ADICT API.
Visit https://grdf-adict.grdf.fr, to know how to get ones. During your
registration you will also give a redirect URL, you will need it to use this library.

Now that you have those, install the library with composer:

```sh
composer require aeneria/grdf-adict
```

If you are using a framework such as Symfony, you can declare the library as a service and
use it with dependency injection:

```yaml
# config/services.yaml

services:
    Aeneria\GrdfAdictApi\Client\DataConnectService:
        class: Aeneria\GrdfAdictApi\Client\GrdfAdictService
        arguments:
            $httpClient: "@http_client"
            $authEndpoint: "https://mon-compte-particulier.enedis.fr"
            $dataEndpoint: "https://gw.prd.api.enedis.fr"
            $clientId: "YOUR_CLIENT_ID"
            $clientSecret: "YOUR_CLIENT_SECRET"
            $redirectUri: "YOUR_REDIRECT_URI"
```

Or you can declare it in your code this way:

```php
<?php

use Aeneria\GrdfAdictApi\Client\GrdfAdictService;

//...

$grdfAdict = new GrdfAdictService(
    HttpClient::create(),
    "https://mon-compte-particulier.enedis.fr",
    "https://gw.prd.api.enedis.fr",
    "YOUR_CLIENT_ID",
    "YOUR_CLIENT_SECRET",
    "YOUR_REDIRECT_URI
);

//...

```

Then you can use the API:

```php
<?php

// Getting consent page URL
$grdfAdict->getAuthentificationClient()->getConsentPageUrl($state, 'yourOrganizationName');

// Getting Token to use the API
$accessToken = $grdfAdict->getAuthentificationClient()->requestAuthorizationToken()


// Getting consumption data
$meteringData = $grdfAdict->getConsommationClient()->requestConsoInformative(
    $accessToken,
    $pce,
    new \DateTimeImmutable('8 days ago'),
    new \DateTimeImmutable('2 days ago')
);

// Getting customer data
$infoTechnique = $dataConnect->grdfAdict->getContratClient()->requestInfoTechnique(
    $accessToken,
    $pce
);

```

## Support

Feel free to [open an issue](https://gitlab.com/aeneria/grdf-adict/-/issues)!
